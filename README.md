# matx

## Project status

Dead.

## Installation

Hopefully impossible.

## Functions

```js
matx.map(w, h, f)
matx.for_each(mt, v0, f)
matx.init(w, h, v)
matx.zeros(w, h)
matx.diag(wh, v1, v0)
matx.width(mt)
matx.height(mt)
matx.is_square(mt)
matx.remap(mt, f)
matx.get(mt, x, y)
matx.set(mt, x, y, e)
matx.mult(a, b, f)
```

## Usage


```js
const matx = require('matx');

// ...

const nines = matx.init(3, 4, 9);

const tens = matx.init(5, 3, 10);

const prod = matx.mult(nines, tens);
```

## Contributing

No contributing is allowed yet.

## License

See LICENSE file for the license information.
