
const matx = require("../src/matx");
const assert = require("assert");

describe("matx", function() {
  describe("zeros", function() {
    it("create an array full of zeros", function() {
      assert.deepStrictEqual(matx.zeros(2, 3),
        [[0, 0],
         [0, 0],
         [0, 0]]);
    });
  });
  describe("init", function() {
    it("create an array full of the same number", function() {
      assert.deepStrictEqual(matx.init(2, 3, 4),
        [[4, 4],
         [4, 4],
         [4, 4]]);
    });
  });
  describe("diag", function() {
    it("create an array with ones on the diagonal and zeros elsewhere", function() {
      assert.deepStrictEqual(matx.diag(3, 1, 0),
        [[1, 0, 0],
         [0, 1, 0],
         [0, 0, 1]]);
    });
  });
  describe("width & height", function() {
    var mt = matx.zeros(3, 2);
    it("matrix width equals 3", function() {
      assert.equal(matx.width(mt), 3);
    });
    it("matrix height equals 2", function() {
      assert.equal(matx.height(mt), 2);
    });
  });
  describe("is_square", function() {
    var mt0 = matx.zeros(3, 3);
    var mt1 = matx.zeros(5, 7);
    it("matrix 3x3 is a square", function() {
      assert.strictEqual(matx.is_square(mt0), true);
    });
    it("matrix 5x7 is not a square", function() {
      assert.strictEqual(matx.is_square(mt1), false);
    });
  });
  describe("remap", function() {
    var mt0 = [[1,2,3],[4,5,6],[7,8,9]];
    var mt1 = [[1,0,0],[0,5,0],[0,0,9]];
    var mt2 = matx.zeros(matx.width(mt0), matx.height(mt0));
    it("only the diagonal is left", function() {
      assert.deepStrictEqual(matx.remap(mt0, (x, y, e) => ((x == y) ? e : 0)), mt1);
    });
    it("set everything to zero", function() {
      assert.deepStrictEqual(matx.remap(mt0, (x, y, e) => 0), mt2);
    });
  });
  describe("for_each", function() {
    var mt0 = matx.zeros(4, 5);
    it("everything is zero in a zero-matrix of any size", function() {
      assert.strictEqual(matx.for_each(mt0, true, (x, y, e, v) => v && (e === 0)), true);
    });
  });
  describe("mult", function() {
    it("a square matrix multiplied with itself", function() {
      const a = [[8, 3, 1], [2, 1, 9], [2, 3, 0]];
      const c = [[72, 30, 35], [36, 34, 11], [22, 9, 29]];
      assert.deepStrictEqual(matx.mult(a, a), c);
    });
    it("nxn multiply nx1", function() {
      const a = [[8, 3, 1], [2, 1, 9], [2, 3, 0]];
      const b = [[1], [2], [3]];
      const c = [[17], [31], [8]];
      assert.deepStrictEqual(matx.mult(a, b), c);
    });
  });
});

