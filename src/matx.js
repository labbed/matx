
var matx = {};

matx.map = function(w, h, f)
{
  var m = [];
  for (let y = 0; y < h; y++) {
    m[y] = [];
    for (let x = 0; x < w; x++) {
      m[y][x] = f(x, y);
    }
  }
  return m;
}

matx.for_each = function(mt, v0, f)
{
  var w = matx.width(mt);
  var h = matx.height(mt);
  var v = v0;
  for (let y = 0; y < h; y++) {
    for (let x = 0; x < w; x++) {
      v = f(x, y, mt[y][x], v);
    }
  }
  return v;
}

matx.init = function(w, h, v)
{
  return matx.map(w, h, (x, y) => v);
}

matx.zeros = function(w, h)
{
  return matx.map(w, h, (x, y) => 0);
}

matx.diag = function(wh, v1, v0)
{
  return matx.map(wh, wh, (x, y) => ((x === y) ? v1 : v0));
}

matx.height = function(mt)
{
  return mt.length;
}

matx.width = function(mt)
{
  return mt[0].length;
}

matx.is_square = function(mt)
{
  return matx.width(mt) === matx.height(mt);
}

matx.remap = function(mt, f)
{
  const w = matx.width(mt);
  const h = matx.height(mt);
  return matx.map(w, h, (x, y) => f(x, y, mt[y][x]));
}

matx.get = function(mt, x, y)
{
  return mt[y][x];
}

matx.set = function(mt, x, y, e)
{
  mt[y][x] = e;
}

matx.mult = function(a, b, f)
{
  const aw = matx.width(a);
  const ah = matx.height(a);
  const bw = matx.width(b);
  const bh = matx.height(b);
  const cw = bw;
  const ch = ah;
  if (aw != bh) {
    throw TypeError(a + " can\'t right multiply " + b);
  }
  return matx.map(cw, ch, function(x, y) {
    var s = 0
    for (let i = 0; i < aw; i++) {
      s += matx.get(a, i, y) * matx.get(b, x, i);
    }
    return s;
  });
}

module.exports = matx;

